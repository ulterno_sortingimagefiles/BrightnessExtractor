#include <iostream>
#include <string>
#include <vector>
#include <sstream>

int getBrightnessHSB (const std::string &pixelDetails, int minval);
int getBrightnessHSBA (const std::string &pixelDetails, int minval);

int main(int argc, char **argv)
{
	std::vector<std::string> input;
	while(true)
	{
		std::string iTemp;
		// std::cin >> iTemp;
		std::getline (std::cin, iTemp);
		input.push_back(std::string (iTemp));
		fflush(stdin);
		if (iTemp.empty())
		{
			break;
		}
	}
	std::cerr << "Got input : \n";
	for (auto in : input)
	{
		std::cerr << in << "\n";
	}

	std::string title = input.at(0);
	if (title.compare(0, 33, "# ImageMagick pixel enumeration: ") == 0)
	{
		title = title.substr (33, title.length() - 33);
		std::cerr << "TITLE: " << title << '\n';	// xpixels,ypixels,rangestart,rangeend,colorspace
		std::stringstream spec_stream(title);
		std::string oneSpec;
		std::getline(spec_stream, oneSpec, ',');
		int xpixels;
		if(!(std::stringstream(oneSpec) >> xpixels))
		{
			std::cerr << "Wrong xpixels : " << oneSpec << '\n';
		}
		std::getline(spec_stream, oneSpec, ',');
		int ypixels;
		if(!(std::stringstream(oneSpec) >> ypixels))
		{
			std::cerr << "Wrong ypixels : " << oneSpec << '\n';
		}
		std::getline(spec_stream, oneSpec, ',');
		int rangestart;
		if(!(std::stringstream(oneSpec) >> rangestart))
		{
			std::cerr << "Wrong rangestart : " << oneSpec << '\n';
		}
		std::getline(spec_stream, oneSpec, ',');
		int rangeend;
		if(!(std::stringstream(oneSpec) >> rangeend))
		{
			std::cerr << "Wrong rangeend : " << oneSpec << '\n';
		}
		std::string colorspace;
		std::getline(spec_stream, colorspace, ',');

		if (xpixels != 1 || ypixels != 1)
		{
			std::cerr << "More than one pixels. Check out what's going on" << '\n';
		}
		if (colorspace.compare("hsb") == 0)
		{
			int ret = getBrightnessHSB(input.at(1), rangestart);
			if (ret < rangestart)
			{
				return ret-rangestart;
			}
			std::cout << ((ret-rangestart)*100)/(rangeend-rangestart);
		}
		else if (colorspace.compare("hsba") == 0)
		{
			int ret = getBrightnessHSBA(input.at(1), rangestart);
			if (ret < rangestart)
			{
				return ret-rangestart;
			}
			std::cout << ((ret-rangestart)*100)/(rangeend-rangestart);
		}
		else
		{

		}
	}
	std::cout << std::endl;
	return 0;
}


int getBrightnessHSB (const std::string &pixelDetails, int minval)
{
	std::stringstream line(pixelDetails);
	std::string tempStr;
	std::getline (line, tempStr, ':');
	if (tempStr.compare("0,0") != 0)
	{
		std::cerr << tempStr;
		return minval-20;
	}
	std::string hsbInts;
	while (hsbInts.empty())
	{
		std::getline (line, hsbInts, ' ');
	}
	std::string hsbHex;
	while (hsbHex.empty())
	{
		std::getline (line, hsbHex, ' ');
	}
	std::string hsbParsed;
	while (hsbParsed.empty())
	{
		std::getline (line, hsbParsed, ' ');
	}
	std::cerr << hsbInts << " | " << hsbHex << " | " << hsbParsed << " | " << '\n';
	try
	{
		std::cerr << line.str().substr(line.tellg(), line.str().length()) << '\n';
	}
	catch (std::out_of_range xEpShn)
	{
		std::cerr << "Formatting matched" << '\n';
	}

	if (hsbInts[0] == '(' && hsbInts[hsbInts.length() -1] == ')')
	{
		std::stringstream hsb_csv(hsbInts.substr(1, hsbInts.length() - 2));
		std::getline(hsb_csv, tempStr, ',');
		int hue; std::stringstream(tempStr) >> hue;
		std::getline(hsb_csv, tempStr, ',');
		int sat; std::stringstream(tempStr) >> sat;
		std::getline(hsb_csv, tempStr, ',');
		int bright; std::stringstream(tempStr) >> bright;
		std::cerr << "HUE: " << hue << "\tSAT: " << sat << "\tBRIGHT: " << bright << std::endl;
		return bright;
	}
	else
	{
		std::cerr << "Well... Not fully\n";
	}

	return minval-10;
}

int getBrightnessHSBA (const std::string &pixelDetails, int minval)
{
	std::stringstream line(pixelDetails);
	std::string tempStr;
	std::getline (line, tempStr, ':');
	if (tempStr.compare("0,0") != 0)
	{
		std::cerr << tempStr;
		return minval-20;
	}
	std::string hsbInts;
	while (hsbInts.empty())
	{
		std::getline (line, hsbInts, ' ');
	}
	std::string hsbHex;
	while (hsbHex.empty())
	{
		std::getline (line, hsbHex, ' ');
	}
	std::string hsbParsed;
	while (hsbParsed.empty())
	{
		std::getline (line, hsbParsed, ' ');
	}
	std::cerr << hsbInts << " | " << hsbHex << " | " << hsbParsed << " | " << '\n';
	try
	{
		std::cerr << line.str().substr(line.tellg(), line.str().length()) << '\n';
	}
	catch (std::out_of_range xEpShn)
	{
		std::cerr << "Formatting matched" << '\n';
	}

	if (hsbInts[0] == '(' && hsbInts[hsbInts.length() -1] == ')')
	{
		std::stringstream hsb_csv(hsbInts.substr(1, hsbInts.length() - 2));
		std::getline(hsb_csv, tempStr, ',');
		int hue; std::stringstream(tempStr) >> hue;
		std::getline(hsb_csv, tempStr, ',');
		int sat; std::stringstream(tempStr) >> sat;
		std::getline(hsb_csv, tempStr, ',');
		int bright; std::stringstream(tempStr) >> bright;
		std::getline(hsb_csv, tempStr, ',');
		int alpha; std::stringstream(tempStr) >> alpha;
		std::cerr << "HUE: " << hue << "\tSAT: " << sat << "\tBRIGHT: " << bright << "\tALPHA: " << alpha << std::endl;
		return bright;
	}
	else
	{
		std::cerr << "Well... Not fully\n";
	}

	return minval-10;
}
